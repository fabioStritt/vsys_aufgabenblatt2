package Aufagbe3e;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

import java.io.IOException;

public class PrimeClient extends Thread {
    private static final String HOSTNAME = "localhost";
    private static final int PORT = 1234;
    private static final String REQUEST_MODE = "c";
    private static final long INITIAL_VALUE = (long) 1e17;
    private static final long COUNT = 20;
    private static final String CLIENT_NAME = PrimeClient.class.getName();

    private Component communication;
    String hostname;
    int port, responsePort;
    long initialValue, count;

    public PrimeClient(String hostname, int port, long initialValue, long count) {
        this.hostname = hostname;
        this.port = port;
        this.initialValue = initialValue;
        this.count = count;
        this.responsePort = (int) (Math.random() * 9999 + 1);
    }

    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " Port: " + responsePort);
            communication = new Component();
            for (long i = initialValue; i < initialValue + count; i++) {
                sendRequest(i);
            }
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void sendRequest(long value) throws IOException, ClassNotFoundException {
        communication.send(new Message(hostname, responsePort, value), port, true);

        Boolean isPrime = (Boolean) communication.receive(responsePort, true, true).getContent();

        System.out.println(Thread.currentThread().getName() + " Requests " + value + ": " + (isPrime ? "prime" : "not prime"));
    }
}
