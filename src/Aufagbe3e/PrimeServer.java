package Aufagbe3e;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrimeServer {
    private final static int PORT=1234;
    private final static Logger LOGGER=Logger.getLogger(PrimeServer.class.getName());

    private Component communication;
    private int port=PORT;
    private Message message;

    ThreadPoolExecutor executor;

    PrimeServer(int port) {
        communication=new Component();
        if(port>0) this.port=port;

        //executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        setLogLevel(Level.FINER);
    }

    private boolean primeService(long number) {
        for (long i = 2; i < Math.sqrt(number)+1; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }

    void setLogLevel(Level level) {
        for(Handler h : LOGGER.getLogger("").getHandlers())	h.setLevel(level);
        LOGGER.setLevel(level);
        LOGGER.info("Log level set to "+level);
    }

    void listen() {
        LOGGER.info("Listening on port "+port);

        while (true) {
            Long request=null;

            LOGGER.finer("Receiving ...");
            try {
                PrimeThread t = new PrimeThread(communication.receive(port, true, true));
                executor.execute(t);

            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int port=0;

        for (int i = 0; i<args.length; i++)  {
            switch(args[i]) {
                case "-port":
                    try {
                        port = Integer.parseInt(args[++i]);
                    } catch (NumberFormatException e) {
                        LOGGER.severe("port must be an integer, not "+args[i]);
                        System.exit(1);
                    }
                    break;
                default:
                    LOGGER.warning("Wrong parameter passed ... '"+args[i]+"'");
            }
        }

        new PrimeServer(port).listen();
    }
}
