package Aufagbe3e;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

public class PrimeThread extends Thread {

    public static int ThreadCount = 0;
    Message message;
    int responsePort;
    Long request;
    Component comm;

    public PrimeThread(Message m) {
        this.message = m;
        comm = new Component();
    }

    @Override
    public void run() {
        System.out.println("Aktuelle Threadzahl: " + ThreadCount++);

        responsePort = message.getPort();
        request = (Long) message.getContent();

        try {
            System.out.println(Thread.currentThread().getName() + " sending to " + message.getPort() + "...");
            comm.send(new Message("localhost", 1234,
                    new Boolean(primeService(request.longValue()))), message.getPort(),true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Aktuelle Threadzahl: " + ThreadCount--);
    }

    private boolean primeService(long number) {
        for (long i = 2; i < Math.sqrt(number)+1; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }
}
