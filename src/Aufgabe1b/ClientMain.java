package Aufgabe1b;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ClientMain extends Thread {
    private static final int port = 1234;
    private static final String hostname = "localhost";
    private static MySocketClient client;
    private static BufferedReader reader;
    private static String name = "";
    private static boolean b = true;


    @Override
    public void run() {

        reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            int random = (int) (Math.random() * 5);
//            System.out.println(random);

//            for (int i = 0; i < random; i++) {
//                Thread.sleep(2000);
//                System.out.print(".");
//            }

            if (reader.ready()) {
                name = reader.readLine();
                b = false;
            }


            System.out.print("Client>");

            System.out.println(Thread.currentThread().getName());
            sendMessage(Thread.currentThread().getName(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {

//        Thread myThread = new ClientMain();
//        myThread.start();

        while (b) {
            try {

                Thread t1 = new ClientMain();
                int sleepCounter = (int) (Math.random() * 4) + 1;
//                System.out.println(sleepCounter);

                t1.start();
                t1.sleep(sleepCounter * 1000);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void sendMessage(String threadName, String clientName) throws Exception {
//        System.out.println("lä#uft");
        client = new MySocketClient(hostname, port, threadName);

        System.out.println(client.sendAndReceive(clientName, threadName));
        client.disconnect();
    }
}
