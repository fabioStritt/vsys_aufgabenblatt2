package Aufgabe1b;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class MySocketServerConnection extends Thread {
    private Socket socket;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;
//    private ObjectInputStream threadNameStream;

    public MySocketServerConnection(Socket socket) throws IOException {
        this.socket = socket;
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectInputStream = new ObjectInputStream(socket.getInputStream());
//        threadNameStream= new ObjectInputStream(socket.getInputStream());
        System.out.println("Server: incoming connection accepted.");
    }

    public void run() {
        System.out.println("Server: waiting for message ...");

        try {
            String string = (String) objectInputStream.readObject();
            String threadName = (String) objectInputStream.readObject();

            System.out.println("Server: received '" + string + "' from: "+threadName);

            objectOutputStream.writeObject("server received '" + string + "' from: "+threadName);

            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
