package Aufgabe2a;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MySocketWebserver {
    private ServerSocket socket;
    private int port;
    private String filename;

    public MySocketWebserver(int port, String filename) throws IOException {
        this.port = port;
        socket = new ServerSocket(port);
        this.filename = filename;
    }

    public void listen() {
        while (true) {
            try {
                System.out.println("Server: listening on port " + port);
                Socket incomingConnection = socket.accept();
                MySocketWebserverConnection connection = new MySocketWebserverConnection(incomingConnection, filename);
                connection.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
