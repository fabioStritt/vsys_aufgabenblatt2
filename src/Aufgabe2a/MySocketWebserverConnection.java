package Aufgabe2a;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class MySocketWebserverConnection extends Thread {
    private Socket socket;

    private Scanner sc;
    private PrintWriter pw;
    private BufferedWriter fileWriter;

    String outputHeaders;
    String outputEndOfHeader = "\r\n\r\n";
    String output;

    String method;
    String path;
    File pathToCheck;

    public MySocketWebserverConnection(Socket socket, String filename) throws IOException {
        this.socket = socket;
        sc = new Scanner(socket.getInputStream());
        pw = new PrintWriter(socket.getOutputStream());
        System.out.println("Server: incoming connection accepted.");

        fileWriter = new BufferedWriter(new FileWriter(filename, true));
    }

    public void run() {
        System.out.println("Server: waiting for message ...");

        try {
            System.out.println("starting..");
            System.out.println("reading..");

            // Methode und Pfad aus Header auslesen, bei Pfad anführendes "/" abschneiden
            method = sc.useDelimiter(" ").next();
            path = sc.next().substring(1);

            writeToFile("Methode: " + method);
            writeToFile("Pfad: " + path);

            pathToCheck = new File(path);


            if (!pathToCheck.exists()) {
                writeToFile("Error: Pfad existiert nicht");
                writeToFile("");

                outputHeaders = "HTTP/1.1 404 Not Found";

                pw.write(outputHeaders + outputEndOfHeader);
            } else {
                outputHeaders = "HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html\r\n" +
                        "Content-Length: ";

                writeToFile("Success: Pfad existiert, Antwort erfolgt");
                writeToFile("");

                output = fileToString(path);

                pw.write(outputHeaders + output.length() + outputEndOfHeader + output);
            }

            fileWriter.close();
            pw.close();
            sc.close();

            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fileToString(String path) throws Exception {

        // GOOD
        return new String(Files.readAllBytes(Paths.get(path)));
    }

    public void writeToFile(String text) throws IOException {
        fileWriter.write(text + "\n");
    }
}

