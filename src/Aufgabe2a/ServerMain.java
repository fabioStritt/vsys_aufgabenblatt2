package Aufgabe2a;

import java.io.IOException;

public class ServerMain {
    private static final int port = 1234;
    private static MySocketWebserver server;

    public static void main(String args[]) {
        try {
            server = new MySocketWebserver(port, "WebserverLog");
            server.listen();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
