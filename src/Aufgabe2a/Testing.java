package Aufgabe2a;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Testing {

    public static void main(String[] args) {

        try {

            BufferedWriter fileWriter = new BufferedWriter(new FileWriter("test"));

            fileWriter.write("leggoo\n");
            fileWriter.write("leggoo2\n");

            File tmp = new File("src/Aufgabe2a/test");
            System.out.println(tmp.exists() ? "gibt" : "gibt nicht");

            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

